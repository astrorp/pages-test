"use strict";

var tables = document.getElementsByClassName("xtable");
var excerciseCount = document.getElementById("exCount").value;
var oldExCount = excerciseCount;

for (var t = 0; t < tables.length; t++) {
	for (var r = 1; r <= excerciseCount; r++) {
		addRow(t);
	}
};

updateResult(); 

function addRow(table) {
	var r = tables[table].rows.length;

	var row = tables[table].insertRow(-1);
	
	var cell1 = row.insertCell(-1);
	cell1.innerHTML = r;

	for (var c = 1; c <= 5; c++) {
		var cell = row.insertCell(-1);
		cell.innerHTML = "<input type=\"checkbox\" id=\"" + table + "-" + r + String.fromCharCode(64 + c) + "\" />";
		cell.children[0].onclick = updateResult;
	}
}

function deleteRow(table) {
	tables[table].deleteRow(-1);
}

function exCountChange() {
	excerciseCount = document.getElementById("exCount").value;
	var diff = excerciseCount - oldExCount;
	oldExCount = excerciseCount;

	if (diff > 0) {
		for (var i = 0; i < diff; i++){
			addRow(0);
			addRow(1);
		}
	}
	if (diff < 0)
	{
		for (var i = 0; i < -diff; i++) {
			deleteRow(0);
			deleteRow(1);
		}
	}

	updateResult();
}

function updateResult() {
	var points = 0;
	var maxPoints = 0;
	for (var r = 1; r <= excerciseCount; r++) {
		var bonus = true;

		for (var c = 1; c <= 5; c++) {
			var marked  = document.getElementById("0-" + r + String.fromCharCode(64 + c)).checked;
			var correct = document.getElementById("1-" + r + String.fromCharCode(64 + c)).checked;

			maxPoints += correct ? 3 : 2;

			if (correct)
				points += marked ? 3 : 0;
			else
				points += marked ? -1 : 2;

			if (marked != correct)
				bonus = false;
		}

		if (bonus)
			points += 2;

		maxPoints += 2;
	}

	points += Number(document.getElementById("extraExPoints").value);
	maxPoints += Number(document.getElementById("extraExMaxPoints").value);

	document.getElementById("points").innerHTML = points;
	document.getElementById("maxPoints").innerHTML = maxPoints;
}

function copyToRight() {
	for (var r = 1; r <= excerciseCount; r++) {

		for (var c = 1; c <= 5; c++) {
			document.getElementById("1-" + r + String.fromCharCode(64 + c)).checked 
				= document.getElementById("0-" + r + String.fromCharCode(64 + c)).checked;
		}
	}

	updateResult();
}

function copyToLeft() {
	for (var r = 1; r <= excerciseCount; r++) {

		for (var c = 1; c <= 5; c++) {
			document.getElementById("0-" + r + String.fromCharCode(64 + c)).checked 
				= document.getElementById("1-" + r + String.fromCharCode(64 + c)).checked;
		}
	}

	updateResult();
}

function clearLeft() {
	for (var r = 1; r <= excerciseCount; r++) {
		for (var c = 1; c <= 5; c++) {
			document.getElementById("0-" + r + String.fromCharCode(64 + c)).checked = false;
		}
	}

	updateResult();
}

function clearRight() {
	for (var r = 1; r <= excerciseCount; r++) {
		for (var c = 1; c <= 5; c++) {
			document.getElementById("1-" + r + String.fromCharCode(64 + c)).checked = false;
		}
	}

	updateResult();
}
